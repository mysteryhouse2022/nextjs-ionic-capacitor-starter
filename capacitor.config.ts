import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'nextjs-ionic-capacitor',
  webDir: 'out',
  bundledWebRuntime: false
};

export default config;
